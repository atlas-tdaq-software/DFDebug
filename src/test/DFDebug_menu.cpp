//******************************************************************************
// file: DFDebug_menu.cpp
// desc: Main help program to desccribe DEBUG_TEXT macro
// auth: ATLAS ROS group. Originally rosdebug_menu.cpp
// modf: 09/01/04 M. Gruwe: New package, new namespace
// modf: 15/01/04 M. Gruwe: Namespace is simply DF
//                          Change enumerator names
// modf: 22/01/04 M. Gruwe: Added RCD and Ttcvi packages 
//******************************************************************************

#include <iostream>
#include <iomanip>
#include <pthread.h>
#include "DFDebug/DFDebug.h"

//using namespace DF;


/***************/
int nesting(void)
/***************/
{
  DEBUG_TEXT(DFDB_DFDB, 1 ,"This is a nested message of level 1"); 
  return(1);
}


/************/
int main(void)
/************/
{
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "This program lists the currently supported packages and their packet IDs" << std::endl;
  std::cout << std::endl;
  std::cout << "------------------------------------------------------------------------" << std::endl;
  std::cout << "Identifier | Description" << std::endl;
  std::cout << "-----------|------------------------------------------------------------" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSFM           << " | ROSFragmentManagement & ROSModules" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSEF           << " | ROSEventFragment" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSSWROBIN      << " | ROSSWRobin" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSFILAR        << " | ROSfilar" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSMEMPOOL      << " | ROSMemoryPool" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSEIM          << " | ROSEventInputManager" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSIO           << " | ROSIO" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSTG           << " | Trigger generator in ROSIO" << std::endl;
  std::cout << std::setw(10) << DFDB_SLINK           << " | ROSslink" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSSOLAR        << " | ROSsolar (SOLAR)" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSQUEST        << " | ROSsolar (QUEST)" << std::endl;
  std::cout << std::setw(10) << DFDB_QUEUE           << " | Queue debugging (ROSSWRobin, ROSIO, ROSCore)" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSCORE         << " | ROSCore" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSROBIN        << " | ROSRobin" << std::endl;
  std::cout << std::setw(10) << DFDB_DFDB            << " | DFDebug (for testing purposes)" << std::endl;
  std::cout << std::setw(10) << DFDB_ROBINIRQ        << " | RobinInterruptCatcher" << std::endl;
  std::cout << std::setw(10) << DFDB_ROSROBINNP      << " | ROSRobinNP" << std::endl;
  std::cout << std::setw(10) << DFDB_CMEMRCC         << " | cmem_rcc" << std::endl;
  std::cout << std::setw(10) << DFDB_IORCC           << " | io_rcc" << std::endl;
  std::cout << std::setw(10) << DFDB_VMERCC          << " | vme_rcc" << std::endl;
  std::cout << std::setw(10) << DFDB_RCCTS           << " | rcc_time_stamp" << std::endl;
  std::cout << std::setw(10) << DFDB_RCCCORBO        << " | rcc_corbo" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDEXAMPLE      << " | RCDExample" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDBITSTRING    << " | RCDBitString" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDMENU         << " | RCDMenu" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDMODULE       << " | RCDModule" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDTTC          << " | RCDTtc" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDVME          << " | RCDVme" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDLTP          << " | RCDLTPModule and RCDLTPConfiguration" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDRODBUSY      << " | rcc_rodbusy and RODBusyModule" << std::endl;
  std::cout << std::setw(10) << DFDB_RCDTTCVIMODULE  << " | RCDTtcviModule" << std::endl;
  std::cout << std::setw(10) << DFDB_RF2TTC          << " | RF2TTC and RFRX modules" << std::endl;
  std::cout << std::setw(10) << DFDB_TTCVI           << " | Ttcvi" << std::endl;
  std::cout << std::setw(10) << DFDB_FELIXCARD       << " | Felix PCIe card" << std::endl;
  std::cout << "-----------|------------------------------------------------------------" << std::endl;
  std::cout << std::endl;
  std::cout << "The debug levels are defined as follows" << std::endl;
  std::cout << "Level | Description" << std::endl;
  std::cout << "------|------------------------------------------------------------" << std::endl;
  std::cout << "    0 | Disable all debugging" << std::endl;
  std::cout << "    5 | Show additional information in case of errors" << std::endl;
  std::cout << "    7 | Show information related to state transitions" << std::endl;
  std::cout << "   10 | Show progression of events flowing through the system" << std::endl;
  std::cout << "   15 | Show more detailed code progession" << std::endl;
  std::cout << "   20 | Show values of parameters" << std::endl;
  std::cout << "------|------------------------------------------------------------" << std::endl;
  std::cout << "Other debug levels are packet specific" << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  
  std::cout << "Now testing the package" << std::endl;
  
  std::cout << "The debug package is DFDB_DFDB" << std::endl;
  std::cout << "The debug level is 0" << std::endl;
  DF::GlobalDebugSettings::setup(0, DFDB_DFDB);
  DEBUG_TEXT(DFDB_DFDB, 1 ,"This is a message of level 1"); 
  DEBUG_TEXT(DFDB_DFDB, 10 ,"This is a message of level 10"); 
  DEBUG_TEXT(DFDB_DFDB, 15 ,"This is a message of level 15"); 
  DEBUG_TEXT(DFDB_DFDB, 20 ,"This is a message of level 20"); 

  std::cout << "The debug package is DFDB_DFDB" << std::endl;
  std::cout << "The debug level is 10" << std::endl;
  DF::GlobalDebugSettings::setup(10, DFDB_DFDB);
  DEBUG_TEXT(DFDB_DFDB, 1 ,"This is a message of level 1"); 
  DEBUG_TEXT(DFDB_DFDB, 10 ,"This is a message of level 10"); 
  DEBUG_TEXT(DFDB_DFDB, 15 ,"This is a message of level 15"); 
  DEBUG_TEXT(DFDB_DFDB, 20 ,"This is a message of level 20"); 
  
  std::cout << "The debug package is DFDB_DFDB" << std::endl;
  std::cout << "The debug level is 20" << std::endl;
  DF::GlobalDebugSettings::setup(20, DFDB_DFDB);
  DEBUG_TEXT(DFDB_DFDB, 1 ,"This is a message of level 1"); 
  DEBUG_TEXT(DFDB_DFDB, 10 ,"This is a message of level 10"); 
  DEBUG_TEXT(DFDB_DFDB, 15 ,"This is a message of level 15"); 
  DEBUG_TEXT(DFDB_DFDB, 20 ,"This is a message of level 20"); 

  std::cout << "The debug package is DFDB_ROSFM" << std::endl;
  std::cout << "The debug level is 20" << std::endl;
  DF::GlobalDebugSettings::setup(20, DFDB_ROSFM);
  DEBUG_TEXT(DFDB_DFDB, 1 ,"This is a message of level 1"); 
  DEBUG_TEXT(DFDB_DFDB, 10 ,"This is a message of level 10"); 
  DEBUG_TEXT(DFDB_DFDB, 15 ,"This is a message of level 15"); 
  DEBUG_TEXT(DFDB_DFDB, 20 ,"This is a message of level 20"); 
  
  
  std::cout << "Testing mutex" << std::endl;
  pthread_mutex_t debug_mutex = PTHREAD_MUTEX_INITIALIZER;
  DF::GlobalDebugSettings::setup(20, DFDB_DFDB);
  pthread_mutex_lock(&debug_mutex);
  DEBUG_TEXT(DFDB_DFDB, 20 ,"This is a message of level 20"); 
  pthread_mutex_unlock(&debug_mutex);

  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "Testing nested macros" << std::endl;
  DEBUG_TEXT(DFDB_DFDB, 20 ,"data = " << nesting()); 

  std::cout << "All tests done." << std::endl;

  std::cout << std::endl;
}
