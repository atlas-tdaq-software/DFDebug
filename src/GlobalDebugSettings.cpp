
/************************************************/
/* file: GlobalDebugSettings.cpp		*/
/* description: Class GlobalDebugSettings	*/
/* maintainer: Markus Joos, CERN/PH-ESS		*/
/************************************************/

#include "DFDebug/GlobalDebugSettings.h"

unsigned int DF::GlobalDebugSettings::s_traceLevel = 0;
unsigned int DF::GlobalDebugSettings::s_packageId  = 0;
std::mutex DF::GlobalDebugSettings::s_debug_mutex;

